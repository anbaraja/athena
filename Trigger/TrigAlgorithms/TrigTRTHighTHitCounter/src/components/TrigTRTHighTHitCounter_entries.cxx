#include "../TrigTRTHTHCounter.h"
#include "../TrigTRTHTHhypo.h"
#include "../TrigTRTHTHCounterMT.h"
#include "../TrigTRTHTHhypoMT.h"
#include "../TrigTRTHTHhypoTool.h"

DECLARE_COMPONENT( TrigTRTHTHCounter )
DECLARE_COMPONENT( TrigTRTHTHhypo )
DECLARE_COMPONENT( TrigTRTHTHCounterMT )
DECLARE_COMPONENT( TrigTRTHTHhypoMT )
DECLARE_COMPONENT( TrigTRTHTHhypoTool )
